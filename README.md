# Astronaut Analysis

This analysis is based on publicly available astronauts data from [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).
In this context, we investigated aspects such as time humans spent in space as well as the age distribution of the astronauts.